package UARTcomm;



import java.util.Random;


import org.jzy3d.analysis.AbstractAnalysis;
import org.jzy3d.analysis.AnalysisLauncher;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.colors.Color;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.rendering.canvas.Quality;


public class Plotter extends AbstractAnalysis{
	
	
	public static void main(String[] args) throws Exception {
		AnalysisLauncher.open(new Plotter());
	}
	
	public Coord3d[] points;
	public Color[] colors;
	public static Scatter scatter;
	
	public Plotter(Coord3d[] p, Color[] c) {
		points = p;
		colors = c;	
	}


	public Plotter() {
	}


	public static void run(Coord3d[] p, Color[] c) throws Exception {
		AnalysisLauncher.open(new Plotter(p, c)  );
	}
		
	
	public static void update() {
		
		scatter.updateBounds();

	}
	
	@Override
    public void init(){

            
        scatter = new Scatter(points, colors);
        
        scatter.setWidth(5);
        
        chart = AWTChartComponentFactory.chart(Quality.Advanced, "newt");
        chart.getScene().add(scatter);
	}
  
}