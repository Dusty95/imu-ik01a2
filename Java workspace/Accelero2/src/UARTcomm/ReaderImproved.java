package UARTcomm;

import java.util.Scanner;

import org.jzy3d.colors.Color;
import org.jzy3d.maths.Coord3d;

import com.fazecast.jSerialComm.SerialPort;

import UARTcomm.Plotter;

public class ReaderImproved {

	public static void main(String[] args) {
		
    	int pointsCounter = 0;
    	int size = 100000;
    	Coord3d[] points = new Coord3d[size];
    	Color[]   colors = new Color[size];
    	boolean start = true;
    	int maxX = -1000;
    	int maxY = -1000;
    	int maxZ = -1000;
    	int minX = 1000;
    	int minY = 1000;
    	int minZ = 1000;
    	
    	int biasX = 0;
    	int biasY = 0;
    	int biasZ = 0;
    	
    	float scaleX = 1;
    	float scaleY = 1;
    	float scaleZ = 1;
    	
    	int r = 0;
    	int g = 0;
    	int b = 255;
    	
    	boolean calibrated = false;
		
		
		SerialPort port = SerialPort.getCommPort("COM4");
		port.setBaudRate(460800);
		port.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 1, 1);
		if(port.openPort() == false) {
			System.err.println("Unable to open the serial port. Exiting.");
			System.exit(1);
		}
		Scanner s = new Scanner(port.getInputStream());
		while(s.hasNextLine()) {
			try {
				String line = s.nextLine();
				String[] token = line.split(" ");
				
				int X = Integer.valueOf(token[0]);
				int Y = Integer.valueOf(token[1]);
				int Z = Integer.valueOf(token[2]);
				
				// AKTUALIZACJA MAKSIM�W I MINIM�W
            	if (X > maxX)
            		maxX = X;
            	if (Y > maxY)
            		maxY = Y;
            	if (Z > maxZ)
            		maxZ = Z;
            	if (X < minX)
            		minX = X;
            	if (Y < minY)
            		minY = Y;
            	if (Z < minZ)
            		minZ = Z;
            	
            	points[pointsCounter] = new Coord3d( scaleX*(X-biasX), scaleY*(Y-biasY), scaleZ*(Z-biasZ));
            	colors[pointsCounter] = new Color(r, g, b);				                	
            	
            	// PO 1000 PROBEK RYSUJEMY SFERE
            	if (pointsCounter == size/10 && start) {
            		
            		for (int i = size/10; i<size; i++) {
                		points[i] = new Coord3d( scaleX*(X-biasX), scaleY*(Y-biasY), scaleZ*(Z-biasZ));
                		colors[i] = new Color(r, g, b);
                	}
            		
            		Plotter.run(points, colors);		                		
            		start = false;           		
            	}
            			                	
            	pointsCounter++;	//ZWIEKSZENIE LICZNIKA ZEBRANYCH PUNKTOW
            	
            	
            	// WYLICZENIE WSPOLCZYNNIKOW KALIBRACJI
            	if (pointsCounter == size/2) {
            			                		                		
            		biasX = (maxX + minX)/2;
            		biasY = (maxY + minY)/2;
            		biasZ = (maxZ + minZ)/2;
            		
            		scaleX = (maxX - minX)/2.0f;
            		scaleY = (maxY - minY)/2.0f;
            		scaleZ = (maxZ - minZ)/2.0f;
            		
            		float avg = (scaleX + scaleY + scaleZ)/3;
            		
            		scaleX = avg/scaleX;
            		scaleY = avg/scaleY;
            		scaleZ = avg/scaleZ;
            		
            		r = 255;
            		b = 0;
            		         
            		calibrated = true;
            		System.out.println("SCALE:  X " + String.valueOf(scaleX) + " Y " + String.valueOf(scaleY) + " Z " +String.valueOf(scaleZ) + "\r\n BIAS:   X " + String.valueOf(biasX) + " Y " + String.valueOf(biasY) + " Z " + String.valueOf(biasZ) + " "  );
            		System.out.println("ZASIEG  X" + String.valueOf(maxX-minX)  + " Y " + String.valueOf(maxY-minY)+ " X " + String.valueOf(maxZ-minZ));
            	
            	}
            	
            	// ZASTEPOWANIE STARYCH PUNKTOW NOWYMI PO ZAPELNIENIU TABLICY
            	if (pointsCounter == size)
            		pointsCounter = 0;
            	
            	//WYPISANIE ODEBRANEGO PUNKTU
            	if (!calibrated) {
            	System.out.println("X = " + String.valueOf(X) + ",Y = " + String.valueOf(Y) +  ",Z = " + String.valueOf(Z) );
            	}
				
				
								
				//System.out.println(token[0] + " " + token[1] + " " + token[2]);
			} catch(Exception e) {}
		}
		s.close();
		System.err.println("Lost communication with the serial port. Exiting.");
		System.exit(1);
		


	}

}
