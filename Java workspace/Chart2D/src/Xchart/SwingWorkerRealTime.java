package Xchart;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.SwingWorker;

import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;

import com.fazecast.jSerialComm.SerialPort;

/**
 * Creates a real-time chart using SwingWorker
 */
public class SwingWorkerRealTime {

  MySwingWorker mySwingWorker;
  SwingWrapper<XYChart> sw;
  JFrame sw2;
  XYChart chart;
  XYChart chart2;
  XYChart chart3;
  List<XYChart> charts = new ArrayList<XYChart>();

  public static void main(String[] args) throws Exception {

    SwingWorkerRealTime swingWorkerRealTime = new SwingWorkerRealTime();
    swingWorkerRealTime.go();
  }

  private void go() {

    // Create Chart
    chart = QuickChart.getChart("Calibrated IMU sensor values - X Axis", "Time", "Value", "GyroscopeX", new double[] { 0 }, new double[] { 0 });
    chart.getStyler().setLegendVisible(true);
    chart.getStyler().setXAxisTicksVisible(true);
    
    chart2 = QuickChart.getChart("Calibrated IMU sensor values - Y Axis", "Time", "Value", "GyroscopeY", new double[] { 0 }, new double[] { 0 });
    chart2.getStyler().setLegendVisible(true);
    chart2.getStyler().setXAxisTicksVisible(true);
    
    chart3 = QuickChart.getChart("Calibrated IMU sensor values - Z Axis", "Time", "Value", "GyroscopeZ", new double[] { 0 }, new double[] { 0 });
    chart3.getStyler().setLegendVisible(true);
    chart3.getStyler().setXAxisTicksVisible(true);
      
	charts.add(chart);
	charts.add(chart2);
	charts.add(chart3);
	
	
	sw2 = new SwingWrapper<XYChart>(charts).displayChartMatrix();
    
    
    // Show it
//    sw = new SwingWrapper<XYChart>(chart);
//    sw.displayChart();
    
    
    mySwingWorker = new MySwingWorker();
    mySwingWorker.execute();
  }

  private class MySwingWorker extends SwingWorker<Boolean, double[]> {

	boolean ok = false;
    LinkedList<Double> fifo1 = new LinkedList<Double>();
    LinkedList<Double> fifo2 = new LinkedList<Double>();
    LinkedList<Double> fifo3 = new LinkedList<Double>();
    
    LinkedList<Double> fifo4 = new LinkedList<Double>();
    LinkedList<Double> fifo5 = new LinkedList<Double>();
    LinkedList<Double> fifo6 = new LinkedList<Double>();
    
    LinkedList<Double> fifo7 = new LinkedList<Double>();
    LinkedList<Double> fifo8 = new LinkedList<Double>();
    LinkedList<Double> fifo9 = new LinkedList<Double>();
    
    //LinkedList<Double[]> fifo = new LinkedList<Double[]>();

    public MySwingWorker() {

      fifo1.add(0.0);
      fifo2.add(0.0);
      fifo3.add(0.0);
      fifo4.add(0.0);
      fifo5.add(0.0);
      fifo6.add(0.0);
      fifo7.add(0.0);
      fifo8.add(0.0);
      fifo9.add(0.0);
    }

    @Override
    protected Boolean doInBackground() throws Exception {

    	SerialPort port = SerialPort.getCommPort("COM4");
		port.setBaudRate(460800);
		port.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 1, 1);
		if(port.openPort() == false) {
			System.err.println("Unable to open the serial port. Exiting.");
			System.exit(1);
		}
		Scanner s = new Scanner(port.getInputStream());
		while(s.hasNextLine()) {
			try {
				String line = s.nextLine();
				String[] token = line.split(" ");
				
				double GX = Float.valueOf(token[0]);
				double AX = Float.valueOf(token[1])/1000;				
				double MX = Float.valueOf(token[2]);		
				
				double GY = Float.valueOf(token[3]);
				double AY = Float.valueOf(token[4])/1000;				
				double MY = Float.valueOf(token[5]);	
				
				double GZ = Float.valueOf(token[6]);
				double AZ = Float.valueOf(token[7])/1000;				
				double MZ = Float.valueOf(token[8]);	
				
				
				System.out.println(GX + " " + AX + " " + MX);
							
				fifo1.add(GX);
		        if (fifo1.size() > 500) {
		          fifo1.removeFirst();
		        }
		        
		        fifo2.add(AX);
		        if (fifo2.size() > 500) {
		          fifo2.removeFirst();
		        }
		        
		        fifo3.add(MX);
		        if (fifo3.size() > 500) {
		          fifo3.removeFirst();
		        }
		        
		        fifo4.add(GY);
		        if (fifo4.size() > 500) {
		          fifo4.removeFirst();
		        }
		        
		        fifo5.add(AY);
		        if (fifo5.size() > 500) {
		          fifo5.removeFirst();
		        }
		        
		        fifo6.add(MY);
		        if (fifo6.size() > 500) {
		          fifo6.removeFirst();
		        }
		        
		        fifo7.add(GZ);
		        if (fifo7.size() > 500) {
		          fifo7.removeFirst();
		        }
		        
		        fifo8.add(AZ);
		        if (fifo8.size() > 500) {
		          fifo8.removeFirst();
		        }

		        fifo9.add(MZ);
		        if (fifo9.size() > 500) {
		          fifo9.removeFirst();
		        }
		        
		        
		        double[][] array = new double[9][fifo1.size()];
		        for (int i = 0; i < fifo1.size(); i++) {
		          array[0][i] = fifo1.get(i);
		          array[1][i] = fifo2.get(i);
		          array[2][i] = fifo3.get(i);
		          array[3][i] = fifo4.get(i);
		          array[4][i] = fifo5.get(i);
		          array[5][i] = fifo6.get(i);
		          array[6][i] = fifo7.get(i);
		          array[7][i] = fifo8.get(i);
		          array[8][i] = fifo9.get(i);
		        }
		        
		        publish(array);

		        try {
		          Thread.sleep(5);
		        } catch (InterruptedException e) {
		          // eat it. caught when interrupt is called
		          System.out.println("MySwingWorker shut down.");
		        }
				
		
				
			} catch(Exception e) {}
		}
		s.close();
		System.err.println("Lost communication with the serial port. Exiting.");
		System.exit(1);
		
		

      return true;
    }

    @Override
    protected void process(List<double[]> chunks) {

//      System.out.println("number of chunks: " + chunks.size());
//      System.out.println( chunks.get( chunks.size() - 1)[0] + "   " + chunks.get( chunks.size() - 2)[1] );

      double[] mostRecentDataSet = chunks.get(chunks.size() - 9);
      double[] mostRecentDataSet2 = chunks.get(chunks.size() - 8);
      double[] mostRecentDataSet3 = chunks.get(chunks.size() - 7);
      
      double[] mostRecentDataSet4 = chunks.get(chunks.size() - 6);
      double[] mostRecentDataSet5 = chunks.get(chunks.size() - 5);
      double[] mostRecentDataSet6 = chunks.get(chunks.size() - 4);
      
      double[] mostRecentDataSet7 = chunks.get(chunks.size() - 3);
      double[] mostRecentDataSet8 = chunks.get(chunks.size() - 2);
      double[] mostRecentDataSet9 = chunks.get(chunks.size() - 1);
      

      if (ok == false) {
    	  ok = true;
      	  chart.addSeries("AccelerometerX", mostRecentDataSet2);
      	  chart.addSeries("MagnetometerX", mostRecentDataSet3);
      	  chart2.addSeries("AccelerometerY", mostRecentDataSet5);
    	  chart2.addSeries("MagnetometerY", mostRecentDataSet6);
    	  chart3.addSeries("AccelerometerZ", mostRecentDataSet8);
    	  chart3.addSeries("MagnetometerZ", mostRecentDataSet9);
       }
      
      chart.updateXYSeries("GyroscopeX", null, mostRecentDataSet, null);
      chart.updateXYSeries("AccelerometerX", null, mostRecentDataSet2, null);
      chart.updateXYSeries("MagnetometerX", null, mostRecentDataSet3, null);
      chart2.updateXYSeries("GyroscopeY", null, mostRecentDataSet4, null);
      chart2.updateXYSeries("AccelerometerY", null, mostRecentDataSet5, null);
      chart2.updateXYSeries("MagnetometerY", null, mostRecentDataSet6, null);
      chart3.updateXYSeries("GyroscopeZ", null, mostRecentDataSet7, null);
      chart3.updateXYSeries("AccelerometerZ", null, mostRecentDataSet8, null);
      chart3.updateXYSeries("MagnetometerZ", null, mostRecentDataSet9, null);
      
      sw2.repaint();

      long start = System.currentTimeMillis();
      long duration = System.currentTimeMillis() - start;
      try {
        Thread.sleep(40 - duration); // 40 ms ==> 25fps
        // Thread.sleep(400 - duration); // 40 ms ==> 2.5fps
      } catch (InterruptedException e) {
      }

    }
  }
}