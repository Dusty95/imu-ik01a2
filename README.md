Madgwick filter implementation for Nucleo F401RE with IKS01A2 shield.

Repo includes:

1) SW4STM project with source code
2) Java app for ploting sensors measurments values
3) Java app for visualising a quaternion

How to use it?

To chose printing option, change value of a constant PRINT_MODE defined in main.h file of 1). Available modes:
- 0 -> no print (no output through UART) - default setting
- 1 -> visualize quaternion - option allowing use of 3)
- 2 -> plot 2D - option allowing use of 2)