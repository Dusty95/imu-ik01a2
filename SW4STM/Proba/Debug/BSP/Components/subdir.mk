################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../BSP/Components/HTS221_Driver.c \
../BSP/Components/HTS221_Driver_HL.c \
../BSP/Components/LIS3MDL_MAG_driver.c \
../BSP/Components/LIS3MDL_MAG_driver_HL.c \
../BSP/Components/LPS22HB_Driver.c \
../BSP/Components/LPS22HB_Driver_HL.c \
../BSP/Components/LPS25HB_Driver.c \
../BSP/Components/LPS25HB_Driver_HL.c \
../BSP/Components/LSM303AGR_ACC_driver.c \
../BSP/Components/LSM303AGR_ACC_driver_HL.c \
../BSP/Components/LSM303AGR_MAG_driver.c \
../BSP/Components/LSM303AGR_MAG_driver_HL.c \
../BSP/Components/LSM6DS0_ACC_GYRO_driver.c \
../BSP/Components/LSM6DS0_ACC_GYRO_driver_HL.c \
../BSP/Components/LSM6DS3_ACC_GYRO_driver.c \
../BSP/Components/LSM6DS3_ACC_GYRO_driver_HL.c \
../BSP/Components/LSM6DSL_ACC_GYRO_driver.c \
../BSP/Components/LSM6DSL_ACC_GYRO_driver_HL.c 

OBJS += \
./BSP/Components/HTS221_Driver.o \
./BSP/Components/HTS221_Driver_HL.o \
./BSP/Components/LIS3MDL_MAG_driver.o \
./BSP/Components/LIS3MDL_MAG_driver_HL.o \
./BSP/Components/LPS22HB_Driver.o \
./BSP/Components/LPS22HB_Driver_HL.o \
./BSP/Components/LPS25HB_Driver.o \
./BSP/Components/LPS25HB_Driver_HL.o \
./BSP/Components/LSM303AGR_ACC_driver.o \
./BSP/Components/LSM303AGR_ACC_driver_HL.o \
./BSP/Components/LSM303AGR_MAG_driver.o \
./BSP/Components/LSM303AGR_MAG_driver_HL.o \
./BSP/Components/LSM6DS0_ACC_GYRO_driver.o \
./BSP/Components/LSM6DS0_ACC_GYRO_driver_HL.o \
./BSP/Components/LSM6DS3_ACC_GYRO_driver.o \
./BSP/Components/LSM6DS3_ACC_GYRO_driver_HL.o \
./BSP/Components/LSM6DSL_ACC_GYRO_driver.o \
./BSP/Components/LSM6DSL_ACC_GYRO_driver_HL.o 

C_DEPS += \
./BSP/Components/HTS221_Driver.d \
./BSP/Components/HTS221_Driver_HL.d \
./BSP/Components/LIS3MDL_MAG_driver.d \
./BSP/Components/LIS3MDL_MAG_driver_HL.d \
./BSP/Components/LPS22HB_Driver.d \
./BSP/Components/LPS22HB_Driver_HL.d \
./BSP/Components/LPS25HB_Driver.d \
./BSP/Components/LPS25HB_Driver_HL.d \
./BSP/Components/LSM303AGR_ACC_driver.d \
./BSP/Components/LSM303AGR_ACC_driver_HL.d \
./BSP/Components/LSM303AGR_MAG_driver.d \
./BSP/Components/LSM303AGR_MAG_driver_HL.d \
./BSP/Components/LSM6DS0_ACC_GYRO_driver.d \
./BSP/Components/LSM6DS0_ACC_GYRO_driver_HL.d \
./BSP/Components/LSM6DS3_ACC_GYRO_driver.d \
./BSP/Components/LSM6DS3_ACC_GYRO_driver_HL.d \
./BSP/Components/LSM6DSL_ACC_GYRO_driver.d \
./BSP/Components/LSM6DSL_ACC_GYRO_driver_HL.d 


# Each subdirectory must supply rules for building sources it contributes
BSP/Components/%.o: ../BSP/Components/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F401xE -DUSE_STM32F4XX_NUCLEO -I"C:/Projects/AC6Workspace/Proba/Inc" -I"C:/Projects/AC6Workspace/Proba/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Projects/AC6Workspace/Proba/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" -I"C:/Projects/AC6Workspace/Proba/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"C:/Projects/AC6Workspace/Proba/BSP/includes/Components" -I"C:/Projects/AC6Workspace/Proba/BSP/includes/STM32F4xx-Nucleo" -I"C:/Projects/AC6Workspace/Proba/BSP/includes/X_NUCLEO_IKS01A2" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Projects/AC6Workspace/Proba/Drivers/CMSIS/Include" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


