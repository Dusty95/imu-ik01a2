################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../BSP/STM32F4xx-Nucleo/stm32f4xx_nucleo.c 

OBJS += \
./BSP/STM32F4xx-Nucleo/stm32f4xx_nucleo.o 

C_DEPS += \
./BSP/STM32F4xx-Nucleo/stm32f4xx_nucleo.d 


# Each subdirectory must supply rules for building sources it contributes
BSP/STM32F4xx-Nucleo/%.o: ../BSP/STM32F4xx-Nucleo/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F401xE -DUSE_STM32F4XX_NUCLEO -I"C:/Projects/AC6Workspace/Proba/Inc" -I"C:/Projects/AC6Workspace/Proba/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Projects/AC6Workspace/Proba/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" -I"C:/Projects/AC6Workspace/Proba/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"C:/Projects/AC6Workspace/Proba/BSP/includes/Components" -I"C:/Projects/AC6Workspace/Proba/BSP/includes/STM32F4xx-Nucleo" -I"C:/Projects/AC6Workspace/Proba/BSP/includes/X_NUCLEO_IKS01A2" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Projects/AC6Workspace/Proba/Drivers/CMSIS/Include" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


