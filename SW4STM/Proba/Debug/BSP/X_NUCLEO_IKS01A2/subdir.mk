################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2.c \
../BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_accelero.c \
../BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_gyro.c \
../BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_humidity.c \
../BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_magneto.c \
../BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_pressure.c \
../BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_temperature.c 

OBJS += \
./BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2.o \
./BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_accelero.o \
./BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_gyro.o \
./BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_humidity.o \
./BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_magneto.o \
./BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_pressure.o \
./BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_temperature.o 

C_DEPS += \
./BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2.d \
./BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_accelero.d \
./BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_gyro.d \
./BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_humidity.d \
./BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_magneto.d \
./BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_pressure.d \
./BSP/X_NUCLEO_IKS01A2/x_nucleo_iks01a2_temperature.d 


# Each subdirectory must supply rules for building sources it contributes
BSP/X_NUCLEO_IKS01A2/%.o: ../BSP/X_NUCLEO_IKS01A2/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F401xE -DUSE_STM32F4XX_NUCLEO -I"C:/Projects/AC6Workspace/Proba/Inc" -I"C:/Projects/AC6Workspace/Proba/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Projects/AC6Workspace/Proba/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" -I"C:/Projects/AC6Workspace/Proba/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"C:/Projects/AC6Workspace/Proba/BSP/includes/Components" -I"C:/Projects/AC6Workspace/Proba/BSP/includes/STM32F4xx-Nucleo" -I"C:/Projects/AC6Workspace/Proba/BSP/includes/X_NUCLEO_IKS01A2" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Projects/AC6Workspace/Proba/Drivers/CMSIS/Include" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


