################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/MadgwickAHRS.c \
../Src/dma.c \
../Src/freertos.c \
../Src/gpio.c \
../Src/i2c.c \
../Src/main.c \
../Src/pb_common.c \
../Src/pb_decode.c \
../Src/pb_encode.c \
../Src/rtc.c \
../Src/stm32f3discovery_msg.pb.c \
../Src/stm32f4xx_hal_msp.c \
../Src/stm32f4xx_hal_timebase_TIM.c \
../Src/stm32f4xx_it.c \
../Src/system_stm32f4xx.c \
../Src/tim.c \
../Src/usart.c 

OBJS += \
./Src/MadgwickAHRS.o \
./Src/dma.o \
./Src/freertos.o \
./Src/gpio.o \
./Src/i2c.o \
./Src/main.o \
./Src/pb_common.o \
./Src/pb_decode.o \
./Src/pb_encode.o \
./Src/rtc.o \
./Src/stm32f3discovery_msg.pb.o \
./Src/stm32f4xx_hal_msp.o \
./Src/stm32f4xx_hal_timebase_TIM.o \
./Src/stm32f4xx_it.o \
./Src/system_stm32f4xx.o \
./Src/tim.o \
./Src/usart.o 

C_DEPS += \
./Src/MadgwickAHRS.d \
./Src/dma.d \
./Src/freertos.d \
./Src/gpio.d \
./Src/i2c.d \
./Src/main.d \
./Src/pb_common.d \
./Src/pb_decode.d \
./Src/pb_encode.d \
./Src/rtc.d \
./Src/stm32f3discovery_msg.pb.d \
./Src/stm32f4xx_hal_msp.d \
./Src/stm32f4xx_hal_timebase_TIM.d \
./Src/stm32f4xx_it.d \
./Src/system_stm32f4xx.d \
./Src/tim.d \
./Src/usart.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F401xE -DUSE_STM32F4XX_NUCLEO -I"C:/Projects/AC6Workspace/Proba/Inc" -I"C:/Projects/AC6Workspace/Proba/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Projects/AC6Workspace/Proba/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" -I"C:/Projects/AC6Workspace/Proba/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"C:/Projects/AC6Workspace/Proba/BSP/includes/Components" -I"C:/Projects/AC6Workspace/Proba/BSP/includes/STM32F4xx-Nucleo" -I"C:/Projects/AC6Workspace/Proba/BSP/includes/X_NUCLEO_IKS01A2" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Projects/AC6Workspace/Proba/Drivers/CMSIS/Include" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Projects/AC6Workspace/Proba/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


