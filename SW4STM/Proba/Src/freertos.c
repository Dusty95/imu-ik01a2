/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include "main.h"
#include "stm32f3discovery_msg.pb.h"
#include <pb_encode.h>
#include <pb_decode.h>
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId readIMUTaskHandle;
osThreadId madgwickTaskHandle;
osTimerId timer100HzHandle;
osSemaphoreId madgwickBinarySemaphoreHandle;

/* USER CODE BEGIN Variables */

#define PI 			3.14159265359
#define GYRO_RATIO  0.07*PI/180.0

#define MAG_BIAS_X	195
#define MAG_BIAS_Y	-45
#define MAG_BIAS_Z	-200
#define MAG_SCALE_X	0.98
#define MAG_SCALE_Y	1.01
#define MAG_SCALE_Z	1.01

extern void *LSM6DSL_X_0_handle;
extern void *LSM6DSL_G_0_handle;
extern void *LSM303AGR_M_0_handle;

extern int16_t offset_x, offset_y, offset_z;

stm32f3discovery_msg msg_encoded = stm32f3discovery_msg_init_zero;
uint8_t buffer[128];
size_t message_length;
bool status;


//osThreadId madgwickTaskHandle;


int INVcount2 = 0;

int countMadgwick = 0;

int suspendSend = 0;


extern UART_HandleTypeDef huart2;

/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartReadIMUTask(void const * argument);
void StartMadgwickTask(void const * argument);
void CallbackTimer100Hz(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */

void StartMadgwickTask(void const * argument);

/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of madgwickBinarySemaphore */
  osSemaphoreDef(madgwickBinarySemaphore);
  madgwickBinarySemaphoreHandle = osSemaphoreCreate(osSemaphore(madgwickBinarySemaphore), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* definition and creation of timer100Hz */
  osTimerDef(timer100Hz, CallbackTimer100Hz);
  timer100HzHandle = osTimerCreate(osTimer(timer100Hz), osTimerPeriodic, NULL);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */

  osTimerStart(timer100HzHandle,10);

  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of readIMUTask */
  osThreadDef(readIMUTask, StartReadIMUTask, osPriorityNormal, 0, 128);
  readIMUTaskHandle = osThreadCreate(osThread(readIMUTask), NULL);

  /* definition and creation of madgwickTask */
  osThreadDef(madgwickTask, StartMadgwickTask, osPriorityHigh, 0, 128);
  madgwickTaskHandle = osThreadCreate(osThread(madgwickTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */

  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* StartReadIMUTask function */
void StartReadIMUTask(void const * argument)
{

  /* USER CODE BEGIN StartReadIMUTask */
  /* Infinite loop */
  for(;;)
  {
//	  printf("IMU");
	  angular_velocity = Gyro_Sensor_Handler(LSM6DSL_G_0_handle,offset_x,offset_y,offset_z,NO_PRINT);
	  acceleration = Accelero_Sensor_Handler(LSM6DSL_X_0_handle,NO_PRINT);
	  if (PRINT_MODE == 3)
		  magnitude = Magneto_Sensor_Handler(LSM303AGR_M_0_handle,PRINT);
	  else
		  magnitude = Magneto_Sensor_Handler(LSM303AGR_M_0_handle,NO_PRINT);

	  osDelay(100);

//	  if (suspendSend != 0)
//		  vTaskResume(sendDataTaskHandle);
	  //vTaskSuspend( NULL );

  }
  /* USER CODE END StartReadIMUTask */
}

/* StartMadgwickTask function */
void StartMadgwickTask(void const * argument)
{
  /* USER CODE BEGIN StartMadgwickTask */

  float r,p,y;
  float * roll;
  float * pitch;
  float * yaw;

  roll = &r;
  pitch = &p;
  yaw = &y;

  /* Infinite loop */
  for(;;)
  {

//	  osDelay(100);
//	  INVcount2 = 1;
//	  vTaskSuspend( NULL );
//	  INVcount2 = 0;


	  if (xSemaphoreTake(madgwickBinarySemaphoreHandle,portMAX_DELAY)) {
		  MadgwickAHRSupdate( (float)angular_velocity.AXIS_X*GYRO_RATIO,(float)angular_velocity.AXIS_Y*GYRO_RATIO,(float)angular_velocity.AXIS_Z*GYRO_RATIO,(float)acceleration.AXIS_X,(float)acceleration.AXIS_Y,(float)acceleration.AXIS_Z,(float)(magnitude.AXIS_X-MAG_BIAS_X)*MAG_SCALE_X,(float)-(magnitude.AXIS_Y-MAG_BIAS_Y)*MAG_SCALE_Y,(float)-(magnitude.AXIS_Z-MAG_BIAS_Z)*MAG_SCALE_Z,roll,pitch,yaw);
	//	  if (PRINT_MODE == 1)
//		  printQuaternions();




		  msg_encoded.acc_x = acceleration.AXIS_X;
		  msg_encoded.acc_y = acceleration.AXIS_Y;
		  msg_encoded.acc_z = acceleration.AXIS_Z;
		  msg_encoded.gyro_x = angular_velocity.AXIS_X;
		  msg_encoded.gyro_y = angular_velocity.AXIS_Y;
		  msg_encoded.gyro_z = angular_velocity.AXIS_Z;
		  msg_encoded.q1 = q0;
		  msg_encoded.q2 = q1;
		  msg_encoded.q3 = q2;
		  msg_encoded.q4 = q3;

		  pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));
		  status = pb_encode(&stream, stm32f3discovery_msg_fields, &msg_encoded);
		  message_length = stream.bytes_written;
		  if (status) {
			  HAL_UART_Transmit_DMA(&huart2, buffer, message_length);
		  }


//		  uint16_t size = sprintf(buffer, "costam hej\r\n");
//		  char buffer2[100] = "test\r\n";
//		  HAL_UART_Transmit_DMA(&huart2, buffer, sizeof(buffer)); //size);

		  countMadgwick++;
		  if (countMadgwick == 100) {

//			  taskENTER_CRITICAL();
//			  char buffer[] = "test\r\n";
//			  HAL_UART_Transmit_DMA(&huart2, buffer, sizeof(buffer));
////			  printf("HALO");
//			  taskEXIT_CRITICAL();
//			  printQuaternions();
			  BSP_LED_Toggle(0);
			  countMadgwick = 0;
		  }
	  }
	  //vTaskResume(readIMUTaskHandle);

  }
  /* USER CODE END StartMadgwickTask */
}

/* CallbackTimer100Hz function */
void CallbackTimer100Hz(void const * argument)
{
  /* USER CODE BEGIN CallbackTimer100Hz */
  
//	printf("CALLBACK");

	static BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xSemaphoreGiveFromISR(madgwickBinarySemaphoreHandle, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );


					//	if (INVcount2 != 0) {
					//		BaseType_t taskYieldRequired  = xTaskResumeFromISR(madgwickTaskHandle);
					//
					//		if (taskYieldRequired == 1) // If the taskYield is reuiqred then trigger the same.
					//		{
					//			taskYIELD();
					//		}
					//	}

  /* USER CODE END CallbackTimer100Hz */
}

/* USER CODE BEGIN Application */
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
