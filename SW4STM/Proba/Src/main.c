/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
#include "dma.h"
#include "i2c.h"
#include "rtc.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */

#include <string.h> /* strlen */
#include <stdio.h>  /* snprintf */
#include <math.h>   /* trunc */

#include "x_nucleo_iks01a2.h"
#include "x_nucleo_iks01a2_accelero.h"
#include "x_nucleo_iks01a2_gyro.h"
#include "x_nucleo_iks01a2_magneto.h"
#include "x_nucleo_iks01a2_pressure.h"
#include "x_nucleo_iks01a2_humidity.h"
#include "x_nucleo_iks01a2_temperature.h"

#include <pb_encode.h>
#include <pb_decode.h>
#include <stm32f3discovery_msg.pb.h>

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

#define PRIGROUP_16G_0S ((const uint32_t) 0x03)
#define PRIGROUP_8G_2S ((const uint32_t) 0x04)
#define PRIGROUP_4G_4S ((const uint32_t) 0x05)
#define PRIGROUP_2G_8S ((const uint32_t) 0x06)
#define PRIGROUP_0G_16S ((const uint32_t) 0x07)

typedef struct displayFloatToInt_s {
  int8_t sign; /* 0 means positive, 1 means negative*/
  uint32_t  out_int;
  uint32_t  out_dec;
} displayFloatToInt_t;

#define MAX_BUF_SIZE 256


static char dataOut[MAX_BUF_SIZE];

uint8_t Received[10];
static uint8_t buffer[128];

//static RTC_HandleTypeDef RtcHandle;
void *LSM6DSL_X_0_handle = NULL;
void *LSM6DSL_G_0_handle = NULL;
//static void *LSM303AGR_X_0_handle = NULL;
void *LSM303AGR_M_0_handle = NULL;
//static void *HTS221_H_0_handle  = NULL;
//static void *HTS221_T_0_handle  = NULL;
//static void *LPS22HB_P_0_handle  = NULL;
//static void *LPS22HB_T_0_handle  = NULL;

int16_t offset_x = 0, offset_y = 0, offset_z = 0;

SemaphoreHandle_t binary_sem = NULL;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

//static void RTC_Config( void );
//static void RTC_TimeStampConfig( void );
static void floatToInt(float in, displayFloatToInt_t *out_value, int32_t dec_prec);
//static void RTC_Handler( void );




void GetODRm(void *handle);
void GetODRa(void *handle);
void GetODRg(void *handle);

void Gyroscope_Calibration(int16_t * offset_x, int16_t * offset_y, int16_t * offset_z);
void Sensor_Initialization();
void cout(uint8_t *ptr);
int Check_USART_Status_Register(int print);

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	if (USART2->SR & USART_SR_ORE) {
		READ_REG(USART2->SR);
		READ_REG(USART2->DR);
	}

//	printf("PRZERWANIE\r\n");

//	 uint8_t Data[40]; // Tablica przechowujaca wysylana wiadomosc.
//
//	 bool status_decoded;
//	 stm32f3discovery_msg msg_decoded = stm32f3discovery_msg_init_zero;
//
//	 pb_istream_t stream_decoded = pb_istream_from_buffer(Received,10);
//	 status_decoded = pb_decode(&stream_decoded, stm32f3discovery_msg_fields, &msg_decoded);
//
//		 /* Check for errors... */
//	 if (!status_decoded)
//		 printf("bad status\r\n");
//	 else
//		 printf("good status\r\n");
//
//	 if(msg_decoded.ld3 == true)
//		 printf("ld3 true\r\n");
//
//	 if(msg_decoded.ld4 == true)
//		 printf("ld4 true\r\n");

	 HAL_UART_Receive_DMA(&huart2, Received, 10);


}

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
//							  binary_sem = xSemaphoreCreateBinary();
//
//							  NVIC_SetPriorityGrouping(PRIGROUP_8G_2S);
//							  uint32_t prio = NVIC_EncodePriority(PRIGROUP_8G_2S,6,0);
//							  NVIC_SetPriority(TIM1_UP_TIM10_IRQn,prio);
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_RTC_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */

  BSP_LED_Init( 0 );

  Sensor_Initialization();

  printf("Gyroscope calibration:");
  Gyroscope_Calibration(&offset_x, &offset_y, &offset_z);
  printf("OFFSET VALUES:\t%d\t%d\t%d",offset_x,offset_y,offset_z);



//  HAL_TIM_Base_Start_IT(&htim10);
//  HAL_TIM_Base_Start_IT(&htim5);

//  BSP_GYRO_Set_ODR_Value(LSM6DSL_G_0_handle, 416.0f );
//  BSP_ACCELERO_Set_ODR_Value(LSM6DSL_X_0_handle, 416.0f);


//  float gyro_fullscale, gyro_sensivity;
//  BSP_GYRO_Get_FS(LSM6DSL_G_0_handle,&gyro_fullscale);
//  BSP_GYRO_Get_Sensitivity(LSM6DSL_G_0_handle,&gyro_sensivity);


//  float r,p,y;
//  float * roll;
//  float * pitch;
//  float * yaw;
//
//  roll = &r;
//  pitch = &p;
//  yaw = &y;
//
//  volatile uint32_t timer_ms = 0;


    stm32f3discovery_msg msg_encoded = stm32f3discovery_msg_init_zero;
    size_t message_length;
    bool status;


//    HAL_UART_Receive_DMA(&huart2, Received, 10);

  /* USER CODE END 2 */

  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();

  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */


//	  GetODRm(LSM303AGR_M_0_handle);
//	  GetODRa(LSM6DSL_X_0_handle);
//	  GetODRg(LSM6DSL_G_0_handle);





//	  msg_encoded.btn_user = true;
//	  msg_encoded.ld3 = false;
//	  msg_encoded.ld4 = true;
//
//	  pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));
//	  status = pb_encode(&stream, stm32f3discovery_msg_fields, &msg_encoded);
//	  message_length = stream.bytes_written;
//	  if (!status)
//		  BSP_LED_Off(0);
//	  else
//		  BSP_LED_On(0);
//
//	  HAL_UART_Transmit_DMA(&huart2,buffer,message_length);
//	  HAL_Delay(500);
//	  BSP_LED_Off(0);
//	  HAL_Delay(500);
//
//	  Check_USART_Status_Register();


  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/* USER CODE BEGIN 4 */


int Check_USART_Status_Register(int print) {

	char str[80];
	strcpy(str, "FLAGS: ");

	int status = 0;

	  uint32_t SR = USART2->SR;
	  if (SR & USART_SR_CTS) {
		  strcat(str, "CTS ");
		  status += 1;
	  }
	  if (SR & USART_SR_LBD) {
		  strcat(str, "LIN ");
		  status += 2;
	  }
	  if (SR & USART_SR_TXE) {
		  strcat(str, "TXE ");
		  status += 4;
	  }
	  if (SR & USART_SR_TC) {
		  strcat(str, "TC ");
		  status += 8;
	  }
	  if (SR & USART_SR_RXNE) {
		  strcat(str, "RXNE ");
		  status += 16;
	  }
	  if (SR & USART_SR_IDLE) {
		  strcat(str, "IDLE ");
		  status += 32;
	  }
	  if (SR & USART_SR_ORE) {
		  strcat(str, "ORE ");
		  READ_REG(USART2->SR);
		  READ_REG(USART2->DR);
		  strcat(str, "ORE ");
		  status += 64;
//		  HAL_UART_Receive_DMA(&huart2, Received, 10);
	  }
	  if (SR & (0x1U << (2U)) ) {
		  strcat(str, "NF ");
		  status += 128;
	  }
	  if (SR & USART_SR_FE) {
		  strcat(str, "FE ");
		  status += 256;
	  }
	  if (SR & USART_SR_PE) {
		  strcat(str, "PE ");
		  status += 512;
	  }

	  if (status != 0)
		  if (print) {
			  printf("%s\r\n", str);
			  while(1) {}
		  }
	  return status;
}

void Sensor_Initialization() {

	  char str[800];
	  strcpy(str, "INITIALIZIATION:");

	  if ( BSP_ACCELERO_Init( LSM6DSL_X_0, &LSM6DSL_X_0_handle ) == COMPONENT_OK ) {
		  strcat(str, "Accelerometer initialized");
	  }
	  if ( BSP_ACCELERO_Sensor_Enable( LSM6DSL_X_0_handle ) == COMPONENT_OK ) {
		  strcat(str, "Accelerometer enabled");
	  }
	  if ( BSP_MAGNETO_Init(LSM303AGR_M_0, &LSM303AGR_M_0_handle) == COMPONENT_OK ) {
		  strcat(str, "Magnetometer initialized");
	  }
	  if ( BSP_MAGNETO_Sensor_Enable( LSM303AGR_M_0_handle ) == COMPONENT_OK ) {
		  strcat(str, "Magnetometer enabled");
	  }
	  if ( BSP_GYRO_Init( LSM6DSL_G_0, &LSM6DSL_G_0_handle ) == COMPONENT_OK ) {
		  strcat(str, "Gyroscope initialized");
	  }
	  if ( BSP_GYRO_Sensor_Enable(LSM6DSL_G_0_handle) == COMPONENT_OK ) {
		  strcat(str, "Gyroscope enabled");
	  }
	  printf("%s",str);

}

void Gyroscope_Calibration(int16_t * offset_x, int16_t * offset_y, int16_t * offset_z) {

	int32_t offX = 0, offY = 0, offZ = 0;

	for (int i = 0; i < 100; i++) {
			Gyro_Sensor_Handler(LSM6DSL_G_0_handle,0,0,0,NO_PRINT);
//			osDelay(10);
			for (int i = 0; i<100000; i++);
	}
	for (int i = 0; i < 100; i++) {
		SensorAxesRaw_t axes = Gyro_Sensor_Handler(LSM6DSL_G_0_handle,0,0,0,NO_PRINT);
		offX += axes.AXIS_X;
		offY += axes.AXIS_Y;
		offZ += axes.AXIS_Z;
//		osDelay(10);
		for (int i = 0; i<100000; i++);
	}
	offX /= 100;
	offY /= 100;
	offZ /= 100;

	*offset_x = (int16_t) offX;
	*offset_y = (int16_t) offY;
	*offset_z = (int16_t) offZ;

}


void printQuaternions() {

	float *q0ptr, *q1ptr, *q2ptr, *q3ptr;
	float q0,q1,q2,q3;

	q0ptr = &q0;
	q1ptr = &q1;
	q2ptr = &q2;
	q3ptr = &q3;

	getQuaternions(q0ptr, q1ptr, q2ptr, q3ptr);



	printf("%2.7f %2.7f %2.7f %2.7f\n\r", q0, q1, q2, q3);


}


static void floatToInt(float in, displayFloatToInt_t *out_value, int32_t dec_prec)
{
  if(in >= 0.0f)
  {
    out_value->sign = 0;
  }else
  {
    out_value->sign = 1;
    in = -in;
  }

  out_value->out_int = (int32_t)in;
  in = in - (float)(out_value->out_int);
  out_value->out_dec = (int32_t)trunc(in * pow(10, dec_prec));
}


int _write(int file, uint8_t *ptr, uint16_t len) {
	//HAL_UART_Transmit(&huart2, ptr, len, 1000);
	HAL_UART_Transmit_DMA(&huart2, ptr, len);
	return len;
}

void cout(uint8_t *ptr) {
	uint16_t msg_lng = sprintf(buffer,ptr);
	HAL_UART_Transmit_DMA(&huart2,buffer,msg_lng);
}

SensorAxesRaw_t Accelero_Sensor_Handler( void *handle, int print )
{

  uint8_t id;
  SensorAxesRaw_t acceleration;
  uint8_t status;

  BSP_ACCELERO_Get_Instance( handle, &id );

  BSP_ACCELERO_IsInitialized( handle, &status );

  if ( status == 1 )
  {
    if ( BSP_ACCELERO_Get_AxesRaw( handle, &acceleration ) == COMPONENT_ERROR )
    {
      acceleration.AXIS_X = 0;
      acceleration.AXIS_Y = 0;
      acceleration.AXIS_Z = 0;
    }
    if (print != 0) {
		snprintf( dataOut, MAX_BUF_SIZE, "%d %d %d\r\n", (int)acceleration.AXIS_X,
				 (int)acceleration.AXIS_Y, (int)acceleration.AXIS_Z );

		HAL_UART_Transmit_DMA( &huart2, ( uint8_t * )dataOut, strlen( dataOut ) );
    }
  }

  return acceleration;

}



SensorAxesRaw_t Magneto_Sensor_Handler( void *handle, int print )
{

  uint8_t id;
  SensorAxesRaw_t magnetic_field;
  uint8_t status;

  BSP_MAGNETO_Get_Instance( handle, &id );

  BSP_MAGNETO_IsInitialized( handle, &status );

  if ( status == 1 )
  {
    if ( BSP_MAGNETO_Get_AxesRaw( handle, &magnetic_field ) == COMPONENT_ERROR )
    {
      magnetic_field.AXIS_X = 0;
      magnetic_field.AXIS_Y = 0;
      magnetic_field.AXIS_Z = 0;
    }
    if (print != 0) {
		snprintf( dataOut, MAX_BUF_SIZE, "%d %d %d\r\n", (int)magnetic_field.AXIS_X,
				 (int)magnetic_field.AXIS_Y, (int)magnetic_field.AXIS_Z );

		HAL_UART_Transmit_DMA( &huart2, ( uint8_t * )dataOut, strlen( dataOut ) );
    }
  }
  return magnetic_field;
}


SensorAxesRaw_t Gyro_Sensor_Handler( void *handle, int16_t offset_x, int16_t offset_y, int16_t offset_z, int print)
{

  uint8_t id;
  SensorAxesRaw_t angular_velocity;
  uint8_t status;

  int32_t  AXIS_Z;

  BSP_GYRO_Get_Instance( handle, &id );

  BSP_GYRO_IsInitialized( handle, &status );

  if ( status == 1 )
  {
    if ( BSP_GYRO_Get_AxesRaw( handle, &angular_velocity ) == COMPONENT_ERROR )
    {
      angular_velocity.AXIS_X = 0;
      angular_velocity.AXIS_Y = 0;
      angular_velocity.AXIS_Z = 0;
    }
    else {
    	//nie ma zabezpieczenia przed przekroczeniem zakresu (zakladam narazie ze tak duzych predkosci nie bedzie)
    	angular_velocity.AXIS_X = (angular_velocity.AXIS_X - offset_x);
    	angular_velocity.AXIS_Y = (angular_velocity.AXIS_Y - offset_y);
    	angular_velocity.AXIS_Z = (angular_velocity.AXIS_Z - offset_z);

    }
    if (print != 0) {

		snprintf( dataOut, MAX_BUF_SIZE, "%d %d %d\r\n",  (int) angular_velocity.AXIS_X ,
				(int) angular_velocity.AXIS_Y,    (int) angular_velocity.AXIS_Z );

		HAL_UART_Transmit_DMA( &huart2, ( uint8_t * )dataOut, strlen( dataOut ) );
    }

  }

  return angular_velocity;

}


void GetODRm(void *handle) {

	float odr;
	displayFloatToInt_t out_value;
	uint8_t id;
	uint8_t status;

	BSP_MAGNETO_Get_Instance( handle, &id );
	BSP_MAGNETO_IsInitialized( handle, &status );

	if ( status == 1 )
	{
		if ( BSP_MAGNETO_Get_ODR( handle, &odr ) == COMPONENT_ERROR )
		{
			snprintf( dataOut, MAX_BUF_SIZE, "MAG ODR[%d]: ERROR\r\n", id );
		}
		else
		{
			floatToInt( odr, &out_value, 3 );
			snprintf( dataOut, MAX_BUF_SIZE, "MAG ODR[%d]: %d.%03d Hz\r\n", (int)id, (int)out_value.out_int, (int)out_value.out_dec );
		}
	}

	HAL_UART_Transmit_DMA( &huart2, ( uint8_t * )dataOut, strlen( dataOut ) );

}

void GetODRa(void *handle) {


	  float odr;
	  uint8_t id;
	  uint8_t status;
	  displayFloatToInt_t out_value;

	  BSP_ACCELERO_Get_Instance( handle, &id );

	  BSP_ACCELERO_IsInitialized( handle, &status );

	if ( status == 1 ) {

		  if ( BSP_ACCELERO_Get_ODR( handle, &odr ) == COMPONENT_ERROR )
	      {
	        snprintf( dataOut, MAX_BUF_SIZE, "ACC ODR[%d]: ERROR\r\n", id );
	      }
	      else
	      {
	        floatToInt( odr, &out_value, 3 );
	        snprintf( dataOut, MAX_BUF_SIZE, "ACC ODR[%d]: %d.%03d Hz\r\n", (int)id, (int)out_value.out_int, (int)out_value.out_dec );
	      }

	}

	HAL_UART_Transmit_DMA( &huart2, ( uint8_t * )dataOut, strlen( dataOut ) );

}



void GetODRg( void *handle )
{

  float odr;
  uint8_t id;
  uint8_t status;
  displayFloatToInt_t out_value;

  BSP_GYRO_Get_Instance( handle, &id );
  BSP_GYRO_IsInitialized( handle, &status );

  if ( status == 1 )
  {
      if ( BSP_GYRO_Get_ODR( handle, &odr ) == COMPONENT_ERROR )
      {
        snprintf( dataOut, MAX_BUF_SIZE, "ODR[%d]: ERROR\r\n", id );
      }
      else
      {
        floatToInt( odr, &out_value, 3 );
        snprintf( dataOut, MAX_BUF_SIZE, "ODR[%d]: %d.%03d Hz\r\n", (int)id, (int)out_value.out_int, (int)out_value.out_dec );
      }

      HAL_UART_Transmit_DMA( &huart2, ( uint8_t * )dataOut, strlen( dataOut ));
   }
}



/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
