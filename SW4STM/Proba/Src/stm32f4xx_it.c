/**
  ******************************************************************************
  * @file    stm32f4xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx.h"
#include "stm32f4xx_it.h"
#include "cmsis_os.h"

/* USER CODE BEGIN 0 */

#define PI 			3.14159265359
#define GYRO_RATIO  0.07*PI/180.0

#define MAG_BIAS_X	195
#define MAG_BIAS_Y	-45
#define MAG_BIAS_Z	-200
#define MAG_SCALE_X	0.98
#define MAG_SCALE_Y	1.01
#define MAG_SCALE_Z	1.01

extern SemaphoreHandle_t binary_sem;
extern osThreadId madgwickTaskHandle;


int INVcount = 0;
extern int INVcount2;

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern DMA_HandleTypeDef hdma_usart2_tx;
extern UART_HandleTypeDef huart2;

extern TIM_HandleTypeDef htim1;

/******************************************************************************/
/*            Cortex-M4 Processor Interruption and Exception Handlers         */ 
/******************************************************************************/

/**
* @brief This function handles System tick timer.
*/
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */
  /* USER CODE END SysTick_IRQn 0 */
  osSystickHandler();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
* @brief This function handles DMA1 stream6 global interrupt.
*/
void DMA1_Stream6_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Stream6_IRQn 0 */

  /* USER CODE END DMA1_Stream6_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart2_tx);
  /* USER CODE BEGIN DMA1_Stream6_IRQn 1 */

  /* USER CODE END DMA1_Stream6_IRQn 1 */
}

/**
* @brief This function handles TIM1 update interrupt and TIM10 global interrupt.
*/
void TIM1_UP_TIM10_IRQHandler(void)
{
  /* USER CODE BEGIN TIM1_UP_TIM10_IRQn 0 */

//
//	uint32_t prio = NVIC_GetPriority(TIM1_UP_TIM10_IRQn);
//	uint32_t mai, sub;
//	uint32_t * maip, subp;
//	maip = &mai;
//	subp = sub;
//	uint32_t group = NVIC_GetPriorityGrouping();
//
//	NVIC_DecodePriority(prio, group,maip,subp);
//
//	if (*maip == 0)
//		BSP_LED_On(0);





//	  i++;
//
//	  if (i > 4) {
//
//		  i = 0;
//		  float r,p,y;
//		  float * roll;
//		  float * pitch;
//		  float * yaw;
//
//		  roll = &r;
//		  pitch = &p;
//		  yaw = &y;


//		MadgwickAHRSupdate( (float)angular_velocity.AXIS_X*GYRO_RATIO,(float)angular_velocity.AXIS_Y*GYRO_RATIO,(float)angular_velocity.AXIS_Z*GYRO_RATIO,(float)acceleration.AXIS_X,(float)acceleration.AXIS_Y,(float)acceleration.AXIS_Z,(float)(magnitude.AXIS_X-MAG_BIAS_X)*MAG_SCALE_X,(float)-(magnitude.AXIS_Y-MAG_BIAS_Y)*MAG_SCALE_Y,(float)-(magnitude.AXIS_Z-MAG_BIAS_Z)*MAG_SCALE_Z,roll,pitch,yaw);
////		MadgwickAHRSupdate( (float)-angular_velocity.AXIS_Y*GYRO_RATIO,(float)-angular_velocity.AXIS_Z*GYRO_RATIO,(float)-angular_velocity.AXIS_X*GYRO_RATIO,(float)-acceleration.AXIS_Y,(float)-acceleration.AXIS_Z,(float)-acceleration.AXIS_X,(float)(magnitude.AXIS_Y-MAG_BIAS_Y)*MAG_SCALE_Y,(float)-(magnitude.AXIS_Z-MAG_BIAS_Z)*MAG_SCALE_Z,(float)-(magnitude.AXIS_X-MAG_BIAS_X)*MAG_SCALE_X,roll,pitch,yaw);
////		  MadgwickAHRSupdate( (float)-angular_velocity.AXIS_Y*GYRO_RATIO,(float)-angular_velocity.AXIS_Z*GYRO_RATIO,(float)-angular_velocity.AXIS_X*GYRO_RATIO,(float)-acceleration.AXIS_Y,(float)-acceleration.AXIS_Z,(float)-acceleration.AXIS_X,0,(float)-(magnitude.AXIS_Z-MAG_BIAS_Z)*MAG_SCALE_Z,0,roll,pitch,yaw);
//
//
//		if (PRINT_MODE == 1)
//			printQuaternions();


//	  }

  /* USER CODE END TIM1_UP_TIM10_IRQn 0 */
  HAL_TIM_IRQHandler(&htim1);
  /* USER CODE BEGIN TIM1_UP_TIM10_IRQn 1 */

  /* USER CODE END TIM1_UP_TIM10_IRQn 1 */
}

/**
* @brief This function handles USART2 global interrupt.
*/
void USART2_IRQHandler(void)
{
  /* USER CODE BEGIN USART2_IRQn 0 */

  /* USER CODE END USART2_IRQn 0 */
  HAL_UART_IRQHandler(&huart2);
  /* USER CODE BEGIN USART2_IRQn 1 */

  /* USER CODE END USART2_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
